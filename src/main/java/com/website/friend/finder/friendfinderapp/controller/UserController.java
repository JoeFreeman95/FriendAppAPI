package com.website.friend.finder.friendfinderapp.controller;


import com.website.friend.finder.friendfinderapp.exceptions.UserServiceException;
import com.website.friend.finder.friendfinderapp.model.request.RequestOperationName;
import com.website.friend.finder.friendfinderapp.model.request.UserDetailsModel;
import com.website.friend.finder.friendfinderapp.model.response.*;
import com.website.friend.finder.friendfinderapp.service.AddressService;
import com.website.friend.finder.friendfinderapp.service.UserService;
import com.website.friend.finder.friendfinderapp.shared.dto.AddressDto;
import com.website.friend.finder.friendfinderapp.shared.dto.UserDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    AddressService addressService;

    @GetMapping(path = "/{id}")
    public UserRest getUser(@PathVariable String id){

        UserRest returnval = new UserRest();
        UserDto userDto = userService.getUserByUserId(id);
        BeanUtils.copyProperties(userDto,returnval);
        return returnval;

    }

    @PostMapping
    public UserRest createUser(@RequestBody UserDetailsModel userDetails) throws UserServiceException {

        //Create a new User Rest as returnvalue
        UserRest returnValue = new UserRest();

        //Check to make sure firstName is now empty in the json body.
        if(userDetails.getFirstName().isEmpty()) throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        // UserDto userDto = new UserDto();
        //BeanUtils.copyProperties(userDetails, userDto);

        //Create a new instance of model mapper
        ModelMapper modelMapper = new ModelMapper();

        //Create UserDTO assigned to model mapper which matched UserDetails to the UserDTO
        UserDto userDto = modelMapper.map(userDetails, UserDto.class);

        // Create the new User
        UserDto createdUser = userService.createUser(userDto);

        //Copy the details from CreatedUser to the Return value;
        modelMapper.map(createdUser,returnValue);

        return returnValue;

    }

    @PutMapping(path = "/{id}")
    public UserRest updateUser(@PathVariable String id,@RequestBody UserDetailsModel userDetailsModel){

        //The return value is of type user rest this will return userId, First Name, Last Name, Email
        UserRest returnValue = new UserRest();

        // Creating the User Data Transfer Object.
        UserDto userDto = new UserDto();

        // Copy the User Details Model to the user DTO.
        BeanUtils.copyProperties(userDetailsModel, userDto);

        //Update the User with the specified user id.
        UserDto updatededUser = userService.updateUser(id,userDto);

        //Copy the updated user details to the userRest return value.
        BeanUtils.copyProperties(updatededUser,returnValue);

        return returnValue;
    }

    @DeleteMapping("/{id}")
    public OperationStatusModel deleteUser(@PathVariable String id){

        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.DELETE.name());

        userService.deleteUser(id);

        returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return returnValue;
    }

    @GetMapping
    public List<UserRest> getUsers(@RequestParam(value = "page", defaultValue = "0")int page,
                                   @RequestParam(value = "limit", defaultValue = "25")int limit){

        //Get list of all users
        List<UserRest> returnValue = new ArrayList<>();

        //Create a list of users which are retrived from the usersService.getusers which takes the page and limit args
        List<UserDto> users = userService.getUsers(page,limit);

        //Create a new instance of model mapper
        ModelMapper modelMapper = new ModelMapper();

        for(UserDto userDto : users){
            UserRest userModel = modelMapper.map(userDto, UserRest.class);
            returnValue.add(userModel);
        }
        return returnValue;
    }

    @GetMapping("{id}/addresses")
    public List<AddressesRest> getUserAddress(@PathVariable String id) {

        List<AddressesRest> returnval = new ArrayList<>();

        ModelMapper modelMapper = new ModelMapper();

        List<AddressDto> addressesDto = addressService.getAddresses(id);

        if (addressesDto != null && !addressesDto.isEmpty()) {
            Type listType = new TypeToken<List<AddressesRest>>() {
            }.getType();
            returnval = modelMapper.map(addressesDto, listType);
        }

        return returnval;
    }
}
