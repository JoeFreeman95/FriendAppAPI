package com.website.friend.finder.friendfinderapp.model.response;

public enum RequestOperationStatus {

    ERROR, SUCCESS
}
