package com.website.friend.finder.friendfinderapp.service;

import com.website.friend.finder.friendfinderapp.shared.dto.AddressDto;

import java.util.List;

public interface AddressService {
    List<AddressDto> getAddresses(String userId);
}
