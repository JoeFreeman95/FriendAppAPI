package com.website.friend.finder.friendfinderapp.service;

import com.website.friend.finder.friendfinderapp.io.entity.UserEntity;
import com.website.friend.finder.friendfinderapp.shared.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService  extends UserDetailsService {

  UserDto createUser(UserDto user);
  UserDto getUser(String email);
  UserDto getUserByUserId(String userId);
  UserDto updateUser(String userId, UserDto user);
  void deleteUser(String userid);
  List<UserDto> getUsers(int page, int limit);
}
