package com.website.friend.finder.friendfinderapp.service.impl;

import com.website.friend.finder.friendfinderapp.exceptions.UserServiceException;
import com.website.friend.finder.friendfinderapp.model.response.ErrorMessage;
import com.website.friend.finder.friendfinderapp.model.response.ErrorMessages;
import com.website.friend.finder.friendfinderapp.repositories.UserRepository;
import com.website.friend.finder.friendfinderapp.io.entity.UserEntity;
import com.website.friend.finder.friendfinderapp.service.UserService;
import com.website.friend.finder.friendfinderapp.shared.Utils;
import com.website.friend.finder.friendfinderapp.shared.dto.AddressDto;
import com.website.friend.finder.friendfinderapp.shared.dto.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepo;

    @Autowired
    Utils utils;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto createUser(UserDto user) {

        if(userRepo.findByEmail(user.getEmail()) != null){
            throw new RuntimeException("Record Already Exists");
        }

        for (int i = 0; i<user.getAddresses().size();i++){

            AddressDto address = user.getAddresses().get(i);
            address.setUserDetails(user);
            address.setAddressId(utils.generateAddressId(30));
            user.getAddresses().set(i,address);
        }

        //Create a new instance of model mapper
        ModelMapper modelMapper = new ModelMapper();

        UserEntity userEntity = modelMapper.map(user,UserEntity.class);

        String publicUserId = utils.generateUserId(30);

        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userEntity.setUserId(publicUserId);
        userEntity.setEmailVerificationStatus(true);
        UserEntity storedUserDetails = userRepo.save(userEntity);

        UserDto returnVal = modelMapper.map(storedUserDetails, UserDto.class);
        return returnVal;
    }

    @Override
    public UserDto getUser(String email){
        UserEntity userEntity = userRepo.findByEmail(email);

        if(userEntity == null) throw new UsernameNotFoundException(email);

        UserDto returnVal = new UserDto();
        BeanUtils.copyProperties(userEntity, returnVal);
        return returnVal;
    }

    @Override
    public UserDto getUserByUserId(String userId) {

        UserDto returnValue = new UserDto();
        UserEntity userEntity =  userRepo.findByUserId(userId);
        if(userEntity == null) throw new UsernameNotFoundException(userId);
        BeanUtils.copyProperties(userEntity,returnValue);
        return returnValue;
    }

    @Override
    public UserDto updateUser(String userId, UserDto user) {
        //Create the Return value.
        UserDto returnValue = new UserDto();

        //Find the user to be updated by their user id.
        UserEntity userEntity =  userRepo.findByUserId(userId);

        //Check to see if the userEntity is empty.
        if(userEntity == null) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

        //Only Update The First name and Last name
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());

        //Save the Changes to the database
         UserEntity updatedUserDetails = userRepo.save(userEntity);

        //Copy the details from updatedUserDetails to the return value(DTO)
        BeanUtils.copyProperties(updatedUserDetails,returnValue);

        return returnValue;
    }

    @Transactional
    @Override
    public void deleteUser(String userid) {

        //Find the user to be updated by their user id.
        UserEntity userEntity =  userRepo.findByUserId(userid);

        //Check to see if the userEntity is empty.
        if(userEntity == null) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

        userRepo.delete(userEntity);
    }

    @Override
    public List<UserDto> getUsers(int page, int limit) {

        //Create a new Array list called returnValue
        List<UserDto> returnValue = new ArrayList<>();

        //Makes the default page 1 instead of 0.
        if(page>0) {
            page = page-1;
        }

        //Create a page request that takes in the page and limit args
        Pageable pageableRequest = PageRequest.of(page,limit);

        // Create a page list using the user repo which is using PagingAndSortingRepository.
        Page<UserEntity> userslistPage = userRepo.findAll(pageableRequest);

        //Creates a user entity list which gets the content from the pagerequest list.
        List<UserEntity> users = userslistPage.getContent();

        //For loop which looks at each user entity in the users list.
        for(UserEntity userEntity : users) {
            //Create a new user DTO to recieve the objects from user entity
            UserDto userDto = new UserDto();

            //Copy the properties from User Entity to the User DTO
            BeanUtils.copyProperties(userEntity,userDto);

            //Add the properties copied into the return value to be returned.
            returnValue.add(userDto);
        }

        return  returnValue;
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        UserEntity userEntity = userRepo.findByEmail(email);

        if(userEntity == null) throw new UsernameNotFoundException(email);

        return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(),new ArrayList<>());
    }
}
