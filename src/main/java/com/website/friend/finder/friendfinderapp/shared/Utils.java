package com.website.friend.finder.friendfinderapp.shared;

import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Random;

@Component
public class Utils {

    private final Random RANDOM = new SecureRandom();
    private final String ALPHA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public String generateUserId(int length){
        return generateRandomString(length);
    }

    public String generateAddressId(int length){
        return generateRandomString(length);
    }

    private String generateRandomString(int length){
        StringBuilder randString = new StringBuilder(length);

        for (int i = 0; i < length; i++){
            randString.append(ALPHA.charAt(RANDOM.nextInt(ALPHA.length())));
        }

        return new String(randString);
    }
}
